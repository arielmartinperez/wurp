# WURP (Wikipedia Universal Random Page)

Web address / dirección web:<br>
<strong>https://arielmartinperez.gitlab.io/wurp/<strong>

<strong>[EN]</strong>

Did you ever wish to have a website that would bring you a random Wikipedia article,
but chosen from all the different language versions of Wikipedia? This little page does exactly that.
There are also buttons that call for random Wikisource books, random Wiktionary entries
and random Wikipedia Commons items, all in a nice button array. Coded by Ariel Martín Pérez
with more than a little help from ChatGPT.

Title font in use: Piscolabis by Ariel Martín Pérez (Tunera Type Foundry)

<strong>[ES]</strong>

¿Alguna vez has deseado tener una página que abra un artículo aleatorio de
Wikipedia, pero elegido entre todas las diferentes versions de idioma de Wikipedia?
Esta pequeña página te permitira hacer eso. También hay botones para acceder a libros aleatorios
de Wikisource, entradas aleatorias de Wiccionario y ficheros aleatorios de Wikipedia Commons,
colocados en una bonita columna de botones. Código escrito por Ariel Martín Pérez
con muchísima ayuda de ChatGPT.

Tipo de letra del título: Piscolabis por Ariel Martín Pérez (Tunera Type Foundry)

## Screenshot

![screenshot](documentation/pictures/wurpscreenshot.png)

## License

WURP is licensed under the GNU General Public License, Version 3.<br>
This license is available with a FAQ at
https://www.gnu.org/licenses/gpl-3.0.html
